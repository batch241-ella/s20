console.log("Hello World!");

let number = Number(prompt("Enter a number:"));
console.log("The number you provided is " + number + ".");

for (let count = number; count > 0; count--) {
	if (count <= 50) {
		console.log("The current value is " + count + ". Terminating the loop...");
		break;
	}

	if (count % 10 === 0) {
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	} else if (count % 10 !== 0 && count % 5 === 0) {
		console.log(count);
	}
}

let longString = "supercalifragilisticexpialidocious";
console.log(longString);

let consonants = "";

for (let i = 0; i < longString.length; i++) {
	if ("aeiou".indexOf(longString[i]) >= 0) {
		continue;
	} else {
		consonants += longString[i];
	}
}

console.log(consonants);